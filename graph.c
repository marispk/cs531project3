//Maris Picher Kanouff
//G01224870

#include "graph.h"

// Initialization Functions
// No vertex: -10
// No edge: -1 (or -5 or -2)
// Vertex with no edges: -5
// Vertex with an edge: -2
// Edge weights: nonnegative weight number
Graph *graph_initialize(){
  int i;
  int j;
  Graph *new = malloc(sizeof(Graph));
  if (new == NULL) { 
    return NULL; 
  }

  new->max_vertex = -1;
  new->adj_matrix[MAX_VERTICES][MAX_VERTICES];
  for (i = 0; i < MAX_VERTICES; i++) {
    for (j = 0; j < MAX VERTICES; j++) {
      new->adj_matrix[i][j] = -10;
    }
  }

  new->visited[MAX_VERTICES];
  for (i = 0; i < MAX_VERTICES; i++) {
    new->visited[i] = 0;
  }

  return new;
}


// Vertex Operations
int graph_add_vertex(Graph *graph, int v1) {
  int i = 0;
  if (graph == NULL) {
    return -1;
  }
  if (v1 < 0 || v1 >= MAX_VERTICES) {
    return -1;
  }
  
  if (!graph_contains_vertex(graph, v1)){
    return 0;
  }

  for (i = 0; i < MAX_VERTICES; i++) {
    graph->adj_matrix[v1, i] = -5;
  }
  
  if (graph->max_vertex < v1) {
    max_vedrtex = v1;
  }

  return 0;
}

int graph_contains_vertex(Graph *graph, int v1){
  if (graph == NULL) {
    return -1;
  }
  if (v1 < 0 || v1 >= MAX_VERTICES) {
    return -1;
  }
  if (graph->adj_matrix[v1, v1] != -10) {
    return 0;
  }
  return -1;
}

int graph_remove_vertex(Graph *graph, int v1){
  int i = 0;
  if (graph == NULL) {
    return -1;
  }
  if (v1 < 0 || v1 >= MAX_VERTICES) {
    return -1;
  }

  if(graph_contains_matrix(graph, v1)) {
    return 0;
  }

  for (i = 0; i < MAX_VERTICES; i++) {
    graph->adj_matrix[i, v1] = -1;
    graph->adj_matrix[v1, i] = -10;
  }

  return 0;
}

// Edge Operations
int graph_add_edge(Graph *graph, int v1, int v2, int wt){
  if (graph == NULL) {
    return -1;
  }
  
  if (v1 < 0 || v1 >= MAX_VERTICES || v2 < 0 || v2 >= MAX_VERTICES) {
    return -1;
  }

  if (!graph_contains_vertex(graph,v1) || !graph_contains_vertex(graph,v2)){
    return -1;
  }

  if (wt <= 0) {
    return -1;
  }
  
  graph->adj_matrix[v1][v2] = wt;
  return 0;
}

int graph_contains_edge(Graph *graph, int v1, int v2){
  if (graph == NULL) {
    return -1;
  }
  if (v1 < 0 || v1 >= MAX_VERTICES || v2 < 0 || v2 >= MAX_VERTICES) {
    return -1;
  
  }
  if (!graph_contains_vertex(graph,v1) || !graph_contains_vertex(graph,v2)){
    return -1;
  }

  if (graph->adj_matrix[v1][v2] > 0) {
    return 0;
  }
  return -1;
}

int graph_remove_edge(Graph *graph, int v1, int v2) {
  if (graph == NULL) {
    return -1;
  }
  if (v1 < 0 || v1 >= MAX_VERTICES || v2 < 0 || v2 >= MAX_VERTICES) {
    return -1;
  }

  if (!graph_contains_vertex(graph,v1) || !graph_contains_vertex(graph,v2)){
    return -1;
  }

  graph->adj_matrix[v1][v2] = -1;
  return 0;
}

// Graph Metrics Operations
int graph_num_vertices(Graph *graph) {
  int i = 0;
  int count = 0;
  if (graph == NULL) {
    return -1;
  }

  if (graph->max_vertex == -1) {
     return 0;
  }

  for (i = 0; i <= graph->max_vertex; i++) {
    if (!graph_contains_vertex(i)){
      count++;
    }
  }
  return count;
}

int graph_num_edges(Graph *graph) {
  int i = 0;
  int j = 0;
  int count = 0;
  if (graph == NULL) {
    return -1;
  }

  if (graph->max_vertex == -1){
    return 0;
  }

  for (i = 0; i <= graph->max_vertex; i++) {
    for (j = 0; j <= graph->max_vertex; j++) {
      if (!graph_contains_edge(i,j)) {
          count++;
      }
    }
  }

  return count;
}

int graph_total_weight(Graph *graph) {
  int i = 0;
  int j = 0;
  int sum = 0;
  if (graph == NULL) {
    return -1;
  }

  if (graph->max_vertex == -1){
    return 0;
  }

  for (i = 0; i <= graph->max_vertex; i++) {
    for (j = 0; j <= graph->max_vertex; j++) {
      if (!graph_contains_edge(i,j)) {
          sum += graph->adj_matrix[i][j];
      }
    }
  }

  return sum;
}

// Vertex Metrics Operations
int graph_get_degree(Graph *graph, int v1) {
  int i = 0;
  int count = 0;
  if (graph == NULL) {
    return -1;
  }

  if (graph->max_vertex == -1) {
    return -1;
  }

  if (graph_contains_vertex(graph, v1)) {
    return -1;
  }

  for (i = 0; i <= graph->max_vertex; i++) {
    if(graph_contains_edge(graph, v1, i)) {
      count++;
    }

    if (graph_contains_edge(graph, i, v1)) {
        count++;
    }

    if (graph_contains_edge(graph, v1, v1)) {
      count--;
    }
  }

  return count;
  
  
  return -1;
}

int graph_get_edge_weight(Graph *graph, int v1, int v2) {
  if (graph == NULL) {
    return -1;
  }
  
  if (graph_contains_edge(graph, v1, v2)) {
    return -1;
  }

  return graph->adj_matrix[v1][v2];
}

int graph_is_neighbor(Graph *graph, int v1, int v2) {
  if (graph == NULL) {
    return 0;
  }
  
  if (graph_contains_edge(graph, v1, v2)) {
    return 0;
  }

  if (graph_contains_edge(graph, v2, v1)) {
    return 0;
  }

  return 1;
}

int *graph_get_predecessors(Graph *graph, int v1) {
  if (graph == NULL) {
    return NULL;
  }
  
  int i = 0;
  int j = 0;
  int *ary = malloc(sizeof(int) * (graph->max_index + 1));

  if (graph_contains_vertex(graph, v1)) {
    return NULL;
  }

  for (i = 0; i <= graph->max_index; i++) {
    if (!graph_contains_edge(graph, i, v1)) {
      ary[j] = i;
      j++;
    }
  }
  ary[j] = -1;

  return ary;
}

int *graph_get_successors(Graph *graph, int v1) {
  if (graph == NULL) {
    return NULL;
  }
  
  int i = 0;
  int j = 0;
  int *ary = malloc(sizeof(int) * (graph->max_index + 1));

  if (graph_contains_vertex(graph, v1)) {
    return NULL;
  }

  for (i = 0; i <= graph->max_index; i++) {
    if (!graph_contains_edge(graph, v1, i)) {
      ary[j] = i;
      j++;
    }
  }
  ary[j] = -1;

  return ary;
}

// Graph Path Operations
int graph_has_path(Graph *graph, int v1, int v2) {
  int i = 0;
  int has_path = 0;
  int *successors = NULL;
  
  if (graph == NULL) {
    return 0;
  }
  if (graph_contains_vertex(graph, v1) || graph_contains_vertex(graph, v2)){
    return 0;
  }
  
  graph->visited[v1] = 1;
  successors = graph_get_successors(graph, v1);

  while(successors[i] != -1) {
    if(successors[i] == v2) {
      return 1;
    }

    has_path = graph_has_path(graph, successor[i], v2);
    if (has_path) {
      free(successors);
      return 1;
    }
    i++;
  }

  free(successors);
  return 0;

}

// Input/Output Operations
void graph_print(Graph *graph) {
  int i = 0;
  int j = 0;
  
  if (graph == NULL) {
    return;
  }

  for (i = 0; i <= graph->max_vertex; i++) {
    for (j = 0; j <= graph->max_vertex; j++) {
      printf("%d ", graph->adjmatrix[i][j]);
    }
    printf("\n");
  }
}

void graph_output_dot(Graph *graph, char *filename) {
  int i = 0;
  int j = 0;
  int *s = NULL;
  
  if (graph == NULL) {
    return; 
  }
  
  printf("digraph {\n");

  for (i = 0; i <= graph->max_vertex; i++) {
    if (!graph_contains_vertex(graph, i)) {
      printf("%d;\n", i);
      j = 0;
      s = graph_get_successors(graph, i);
      if (s == NULL) {
        return;
      }
      while(s[j] != -1) {
        printf("%d -> %d [label = %d];\n", i, s[j], graph->adj_matrix[i][s[j]]);
        j++;
        free(s);
      }
    }
  }

  printf("}\n");
}

int graph_load_file(Graph *graph, char *filename) {
  return -1;
}

int graph_save_file(Graph *graph, char *filename) {
  return -1;
}

